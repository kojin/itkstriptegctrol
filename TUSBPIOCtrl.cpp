#include "stdafx.h"
#include "TUSBPIOCtrl.h"
#include <iostream>

TUSBPIOCtrl::TUSBPIOCtrl() {
    Initialize();
}
TUSBPIOCtrl::TUSBPIOCtrl(int id) {
    DevId = id;
    Initialize();
    std::cout << "successfully initialised" << std::endl;
}

int TUSBPIOCtrl::Initialize() {
    PortABitmap = 0;
    PortBBitmap = 0;
    PortCBitmap = 0;
    OpenDevice();
    SetAlloutput();
    return 0;
}
int TUSBPIOCtrl::OpenDevice() {
    short RetCode;

    //***デバイスのオープン***
    RetCode = Tusbpio_Device_Open(DevId);
    if (RetCode == 0)
    {
        std::cout << "Device has been opened successfully !" << std::endl;
    }
    else
    {
        std::cerr << "Device could not open properly... " << std::endl;
    }
    return RetCode;
}
void TUSBPIOCtrl::CloseDevice() {
    Tusbpio_Device_Close(DevId); //***デバイスクローズ
}
int TUSBPIOCtrl::SetAlloutput() {
    short RetCode;
    unsigned char ModeBitmap;

    ModeBitmap = 0x80; //モード設定 mode 0
    std::cout << std::hex << ModeBitmap << std::endl;



    RetCode = Tusbpio_Dev1_Write(DevId, 3, ModeBitmap);
    if (RetCode != 0)
        std::cerr << "mode set failed... " << std::endl;
    return RetCode;
}
int TUSBPIOCtrl::SetSWon(int port) {
    unsigned char bit;
    if (port == 0)bit = PortABitmap;
    else if (port == 1)bit = PortBBitmap;
    else if (port == 2)bit = PortCBitmap;
    short RetCode = Tusbpio_Dev1_Write(DevId, port, bit);

    return 0;
}
int TUSBPIOCtrl::SetSWoff() {
    PortABitmap = 0;
    PortBBitmap = 0;
    PortCBitmap = 0;
    short RetCode;
    RetCode = Tusbpio_Dev1_Write(DevId, 0, PortABitmap);
    RetCode = Tusbpio_Dev1_Write(DevId, 1, PortBBitmap);
    RetCode = Tusbpio_Dev1_Write(DevId, 2, PortCBitmap);
    return 0;
}
