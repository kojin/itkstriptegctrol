#include "stdafx.h"
#include "SerialCom.h"
SerialCom::SerialCom() {
    dev = 0;
    initialized = false;
    //    baudrate = B115200;
}

SerialCom::SerialCom(std::string deviceName) {
    dev = 0;
    //    baudrate = B115200;
    this->init(deviceName);
}

SerialCom::~SerialCom() {
    //   if (dev)
    //       _close(dev);
    PxSerialClose();

}

void SerialCom::init(std::string deviceName) {
    //    dev = open(deviceName.c_str(), O_RDWR | O_NOCTTY);
    dev = PxSerialOpen(deviceName.c_str());
    if (dev != 0)
    {
        printf("Error %08x opening %s.\n", dev, deviceName);
        return;
    }
    else {
        std::cout << "successfully opened................." << std::endl;
    }

    this->config();
}

void SerialCom::config() {
    /*
    fcntl(dev, F_SETFL, 0);

    try {
      if (tcgetattr(dev, &tty)) {
        std::cerr << "[ERROR] " << __PRETTY_FUNCTION__ << " : Could not get tty attributes!" << std::endl;
        std::cerr << "[ERROR] " << __PRETTY_FUNCTION__ << " : You may need root privilege." << std::endl;
        throw "No access to device.";
      }
    }
    catch(...){
      return;
    }
    //    tty_old = tty;

    cfsetospeed(&tty, baudrate);
    cfsetispeed(&tty, baudrate);

    tty.c_cflag &= ~PARENB; // no parity
    tty.c_cflag &= ~CSTOPB; // one stop bit
    tty.c_cflag &= ~CRTSCTS; // no flow control
    tty.c_cflag &= ~CSIZE;
    tty.c_cflag |= CS8; // 8 bits
    tty.c_cflag |= CREAD | CLOCAL;

    //tty.c_cc[VMIN]   =  0;
    //tty.c_cc[VTIME]   =  0.5;

    cfmakeraw(&tty);
    tty.c_lflag |= ICANON;

    tcflush(dev, TCIFLUSH);
    if (tcsetattr(dev, TCSANOW, &tty))
        std::cerr << "[ERROR] " << __PRETTY_FUNCTION__ << " : Could not set tty attributes!" << std::endl;
    */
    initialized = true;
}

int SerialCom::write(char* buf, size_t length) {
    //   return ::write(dev, buf, length);
    DWORD  m_written = 0;
    return PxSerialWrite(buf, length, &m_written);
}

int SerialCom::read(char* buf, size_t length) {
    //    return ::read(dev, buf, length);
    DWORD* const m_read = 0;
    return PxSerialRead(buf, length, m_read);
}

int SerialCom::write(std::string bufstr) {
    //std::cout << __PRETTY_FUNCTION__ << " : " << buf << std::endl;
//    return ::write(dev, buf.c_str(), buf.size());
    const char* bufc = bufstr.c_str();
    DWORD m_written = 0;
    return PxSerialWrite((char*)bufc, bufstr.size(), &m_written);
}

int SerialCom::read(std::string& buf) {
    char* tmp = new char[MAX_READ];
    //    unsigned n_read = ::read(dev, tmp, MAX_READ);
    DWORD* const m_read = 0;
    unsigned n_read = PxSerialRead(tmp, MAX_READ, m_read);

    buf = std::string(tmp, n_read);
    //std::cout << __PRETTY_FUNCTION__ << " : " << buf << std::endl;
    return n_read;
}
