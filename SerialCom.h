////////////////////////////////////////////////////////
// Developped by LBL for Prologix GPIB-USB Controller
////////////////////////////////////////////////////////
#ifndef SERIALCOM_H
#define SERIALCOM_H

#include <iostream>
#include <string>
//#include <fcntl.h>
//#include "unistd.h"
//#include "termios.h"
#include "ComPort.h"

class SerialCom {
    public:
        SerialCom();
        SerialCom(std::string deviceName);
        ~SerialCom();

        void init(std::string deviceName);
        void config();

        int write(char *buf, size_t length);
        int write(std::string bufstr);
        int read(char *buf, size_t length);
        int read(std::string &buf);
        int read1(char *buf1, size_t length);
        int read1(std::string &buf1);
        int read2(char *buf2, size_t length);
        int read2(std::string &buf2);


        bool is_initialized(){return initialized;};

    private:
        const unsigned MAX_READ = 4096;
        int dev;
        bool initialized;

 //       speed_t baudrate;
 //       struct termios tty;
        //        struct termios tty_old;
};


#endif
