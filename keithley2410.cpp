#include<iostream>
#include<chrono>
#include<thread>
#include<cmath>
#include "stdafx.h"
#include"serial_interface.h"
#include"keithley2410.h"
#include"SerialCom.h"
#include<io.h>

int keithley2410::initialize()
{
    return 0;
}

int keithley2410::finalize()
{
    return 0;
}

int keithley2410::reset(serial_interface* si)
{
    si->set_address(m_address);
    si->write("*RST\r\n");
    return 0;
}

int keithley2410::configure(serial_interface* si)
{
    si->set_address(m_address);
    si->make_listener();

    //Making Voltage source mode with fixed Voltage
    si->write(":SOURCE:FUNCTION VOLTAGE\r\n");
    si->write(":SOURCE:VOLTAGE:MODE FIXED\r\n");
    si->write(":SOURCE:VOLTAGE:RANGE MAX\r\n");

    //Measurement settings
    si->write(":TRIGGER:COUNT 1\r\n");
    si->write(":SENSE:CURRENT:RANGE 10E-6\r\n");
    si->write(":SENSE:FUNCTION \"CURR\"\r\n");
    si->write(":SENSE:FUNCTION \"VOLT\"\r\n");
    si->write(":FORMAT:ELEMENTS VOLTAGE,CURRENT\r\n");

    //Configuring Voltage and the current compliance.
    si->write(":SENSE:CURRENT:PROTECTION " + std::to_string(m_compliance) + "\r\n");
    si->write(":SOURCE:VOLTAGE:LEVEL " + std::to_string(m_voltage) + "\r\n");

    return 0;
}

int keithley2410::power_on(serial_interface* si)
{
    si->set_address(m_address);
    si->write("OUTPUT ON\r\n");

    return 1;
}

int keithley2410::power_off(serial_interface* si)
{
    si->set_address(m_address);
    si->write("OUTPUT OFF\r\n");
    return 1;
}

int keithley2410::voltage_sweep(serial_interface* si)
{
    si->set_address(m_address);

    double applied_voltage = this->read_voltage(si);
    //  std::cout<<"Target voltage: "<<m_voltage<<"[V]"<<std::endl;
    //std::cout<<"Applied voltage: "<<applied_voltage<<"[V]"<<std::endl;
    //std::cout<<"Voltage step size: "<<(m_voltage-applied_voltage)/m_sweep_steps<<"[V]"<<std::endl;
    //std::cout<<"#steps: "<<m_sweep_steps<<std::endl;

    for (int istep = 0; istep < m_sweep_steps; istep++) {
        double tmp_voltage = round(applied_voltage + (m_voltage - applied_voltage) / m_sweep_steps * (istep + 1));
        if (istep == m_sweep_steps - 1) tmp_voltage = m_voltage;
        si->write(":SOURCE:VOLTAGE:LEVEL " + std::to_string(tmp_voltage) + "\r\n");
        std::cout << "#" << istep << ": Applied voltage: " << this->read_voltage(si) << "[V]" << std::endl;
        std::this_thread::sleep_for(std::chrono::milliseconds(m_sweep_sleep_in_ms));
    }

    return 0;
};

int keithley2410::config_voltage(serial_interface* si)
{
    si->set_address(m_address);
    si->write(":SOURCE:VOLTAGE:LEVEL " + std::to_string(m_voltage) + "\r\n");
    return 0;
}

int keithley2410::config_compliance(serial_interface* si)
{
    si->set_address(m_address);
    si->write(":SENSE:CURRENT:PROTECTION " + std::to_string(m_compliance) + "\r\n");
    return 0;
}

bool keithley2410::is_on(serial_interface* si)
{
    std::string buffer;
    si->set_address(m_address);
    si->write(":OUTPUT?\r\n");
    si->make_talker();
    si->read(buffer);
    si->make_listener();
    if (buffer.substr(0, 1) == "0") return false;
    return true;
}

bool keithley2410::is_off(serial_interface* si)
{
    return (! this->is_on(si));
}

double keithley2410::read_voltage(serial_interface* si)
{
    std::string buffer;
    si->set_address(m_address);
    si->write(":READ?\r\n");
    si->make_talker();
    si->read(buffer);
    double voltage = std::stod(buffer.substr(0, 13));
    si->make_listener();

    return voltage;
}

double keithley2410::read_current(serial_interface* si)
{
    std::string buffer;
    si->set_address(m_address);
    si->write(":READ?\r\n");
    si->make_talker();
    si->read(buffer);
    double current = std::stod(buffer.substr(14, 13));
    si->make_listener();

    return current;
}

void keithley2410::read_voltage_and_current(serial_interface* si, double& voltage, double& current)
{
    std::string buffer;
    si->set_address(m_address);
    si->write(":READ?\r\n");
    si->make_talker();
    si->read(buffer);
    std::cout << buffer << std::endl;
    voltage = std::stod(buffer.substr(0, 13));
    current = std::stod(buffer.substr(14, 13));
    si->make_listener();
    return;
}


void keithley2410::read_voltage_and_current2(serial_interface* si, double& voltage, double& current)
{
    std::string buffer1;
    si->set_address(m_address);
    si->write(":READ?\r\n");
    si->make_talker();
    si->read1(buffer1);
    //std::cout<<buffer1<<std::endl;
    voltage = std::stod(buffer1.substr(0, 13));
    current = std::stod(buffer1.substr(14, 13));
    si->make_listener();

    return;
}