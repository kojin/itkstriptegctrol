// serial_interface.h
#ifndef INCLUDED_SERIAL_INTERFACE
#define INCLUDED_SERIAL_INTERFACE
#include<string>

class serial_interface
{
 public:
  serial_interface(){};
  virtual ~serial_interface(){};

  virtual int initialize() { return 0; };
  virtual int finalize() { return 0; };

  bool is_initialized(){return m_initialized;};
  int get_address(){return m_address;};
  std::string get_device_name(){return m_device_name;};

  virtual int set_address(int address) { return 0; }; //This should be implemented in a daughter class.
  void set_device_name(std::string device_name){m_device_name = device_name;};

  virtual int write(std::string command) { return 0; };
  virtual std::string read(std::string& buffer) { return "0"; };
  //  virtual std::string read2(std::string &buffer1, std::strinf &buffer2){};
  //  virtual std::string read(std::string &buffer2){};
  virtual std::string read1(std::string &buffer1) { return "0"; };
  virtual std::string read2(std::string &buffer2) { return "0"; };

  virtual int make_listener() { return 0; };
  virtual int make_talker() { return 0; };

 protected:
  int m_address;
  std::string m_device_name;
  int m_initialized;

};

#endif
