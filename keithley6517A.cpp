#include<iostream>
#include<chrono>
#include<thread>
#include<cmath>
#include"stdafx.h"

#include"serial_interface.h"
#include"keithley6517A.h"
#include"SerialCom.h"


int keithley6517A::initialize()
{
    return 0;
}

int keithley6517A::finalize()
{
    return 0;
}

int keithley6517A::reset(serial_interface* si)
{
    si->set_address(m_address);
    si->make_listener();
    si->write("*RST\r\n");
    return 0;
}
/*

int keithley6517A::configure(serial_interface *si)
{
  si->set_address(m_address);
  si->make_listener();
  //Making Voltage source mode
  //a si->write(":SOURCE:VOLT:RANGE >100\r\n");//error-101
  si->write(":SOURCE:VOLT:MCONNECT ON\r\n");// ammeter

  //Measurement settings
  si->write(":SENSE:FUNCTION 'CURRENT'\r\n");
  si->write(":SENSE:FUNCTION 'VOLTAGE'\r\n");
  si->write(":SENSE:CURRENT:RANGE:AUTO ON\r\n");

  si->write(":DATA?");
  //a si->write(":SOURCE:CURRENT:LIMIT ON");error -104
  //Does this command exist...?
  //si->write(":SYST:ZCHECK ON\r\n");// zero-check off

  // //Configuring Voltage and the current compliance.
  //si->write(":SENSE:CURRENT:PROTECTION "+std::to_string(m_compliance)+"\r\n");
  //si->write(":SOURCE:VOLTAGE "+std::to_string(m_voltage)+"\r\n");


  return 0;
}
*/


int keithley6517A::configure(serial_interface* si)
{
    si->set_address(m_address);
    si->write(":SOUR:VOLT:MCON ON\r\n");
    si->write(":SOUR:VOLT:RANGE 1000\r\n");
    si->write(":SENSE:FUNCTION \'CURRENT\'\r\n");
    //  si->write(":SENSE:CURRENT:RANGE 2e-5\r\n");
    //  si->write(":SENSE:CURRENT:RANGE:AUTO ON\r\n");
    si->write(":SYST:ZCHECK ON\r\n");
    Sleep(1000);
    si->write(":SYST:ZCORRECT ON\r\n");
    si->write(":SYST:ZCHECK OFF\r\n");
    //  si->write(":SOURCE:CURRENT:RLIMIT:STATE ON\r\n"); //current compliance?
    si->make_listener();
    //si->write(":SENSE:CURRENT:PROTECTION "+std::to_string(m_compliance)+"\r\n");
    return 0;
}




int keithley6517A::power_on(serial_interface* si)
{
    si->set_address(m_address);
    si->write("OUTPUT ON\r\n");

    return 1;
}


int keithley6517A::power_off(serial_interface* si)
{
    si->set_address(m_address);
    si->write("OUTPUT OFF\r\n");
    return 1;
}

int keithley6517A::voltage_sweep(serial_interface* si)
{
    si->set_address(m_address);
    for (int istep = 0; istep < m_sweep_steps; istep++) {
        double tmp_voltage = m_voltage - m_step_voltage + m_step_voltage / m_sweep_steps * (istep + 1);
        if (istep == m_sweep_steps - 1) tmp_voltage = m_voltage;
        si->write(":SOURCE:VOLTAGE " + std::to_string(tmp_voltage) + "\r\n");
        std::this_thread::sleep_for(std::chrono::milliseconds(m_sweep_sleep_in_ms));
    }

    return 0;
};
/*
int keithley6517A::config_voltage(serial_interface *si)
{
  si->set_address(m_address);
  si->write(":SOURCE:VOLTAGE "+std::to_string(m_voltage)+"\r\n");
  return 0;
}
*/
int keithley6517A::config_compliance(serial_interface* si)
{
    si->set_address(m_address);
    si->make_listener();
    si->write(":SENSE:CURRENT:PROTECTION " + std::to_string(m_compliance) + "\r\n");
    return 0;
}

bool keithley6517A::is_on(serial_interface* si)
{
    std::string buffer;
    si->set_address(m_address);
    si->make_listener();
    si->write(":OUTPUT?\r\n");
    si->make_talker();
    si->read(buffer);
    si->make_listener();
    if (buffer.substr(0, 1) == "0") return false;
    return true;
}

bool keithley6517A::is_off(serial_interface* si)
{
    return (!this->is_on(si));
}

/*
double keithley6517A::read_voltage(serial_interface *si)
{
  std::string buffer;
  si->set_address(m_address);
  si->write(":MEASURE:CURRENT?\r\n");
  si->make_talker();
  si->read(buffer);
  double voltage = std::stod(buffer.substr(0,13));
  si->make_listener();

  return voltage;
}
*/


double keithley6517A::read_voltage(serial_interface* si)
{
    std::string buffer;
    si->set_address(m_address);
    si->make_listener();
    //si->write(":SOUR:VOLT:RANG: 100\r\n");
    //si->write(":SOURCE:VOLTAGE 1000\r\n");

    //  si->write(":SYST:ZCHECK OFF\r\n");// zero-check on
    //  si->write(":SOUR:VOLT:MCON ON\r\n");// ammeter
    //  si->write(":MEASURE:VOLT?\r\n");
    si->write(":SENS:FUNC \'VOLT\'\r\n");
    si->write(":SENS:VOLT:RANG:AUTO ON\r\n");
    si->write(":SENS:DATA?\r\n");
    si->make_talker();
    si->read(buffer);
    std::cout << buffer << std::endl;
    double voltage = std::stod(buffer.substr(0, 13));
    //  current = std::stod(buffer.substr(14,13));
    si->make_listener();
    return 0.0;
}


double keithley6517A::read_current(serial_interface* si)
{
    std::string buffer;
    si->set_address(m_address);
    si->write(":SOUR:VOLT:MCON ON\r\n");
    //si->write(":SOUR:VOLT:RANGE 1000");
    si->write(":SENSE:FUNCTION \'CURRENT\'\r\n");
    si->write(":SENSE:CURRENT:RANGE:AUTO ON\r\n");
    //  si->write(":MEASURE:CURRENT?\r\n");
    si->write(":SENSE:DATA?\r\n");
    si->make_talker();
    si->read(buffer);
    double current = std::stod(buffer.substr(0, 13));
    si->make_listener();

    return current;
}



//can NOT read voltage and current simultaneously. this is like "read current", but you need to prepare "configure" command. "
void keithley6517A::read_voltage_and_current(serial_interface* si, double& voltage, double& current)
{
    std::string buffer;
    si->set_address(m_address);
    //  si->write(":MEASURE:CURRENT?\r\n");
    si->write(":DATA?\r\n");
    si->make_talker();
    si->read(buffer);
    //  std::cout<<buffer<<std::endl;
    //  voltage = std::stod(buffer.substr(0,13));
    current = std::stod(buffer.substr(0, 13));
    si->make_listener();

    return;
}


void keithley6517A::read_voltage_and_current2(serial_interface* si, double& voltage, double& current)
{
    std::string buffer1;
    si->set_address(m_address);
    //  si->write(":MEASURE:CURRENT?\r\n");
    si->write(":SENS:DATA?\r\n");
    si->make_talker();
    si->read(buffer1);
    //voltage = std::stod(buffer1.substr(0,13));
    current = std::stod(buffer1.substr(0, 13));
    si->make_listener();

    return;
}



int keithley6517A::config_voltage(serial_interface* si)
{
    si->set_address(m_address);
    //  si->write(":SOUR:VOLT:RANGE 1000");
    si->write(":SOURCE:VOLTAGE " + std::to_string(m_voltage) + "\r\n");
    //  si->write(":OUTPUT:STAT ON");
    si->make_listener();
    return 0;
}