﻿// ITkStripTEGCtrl.cpp : このファイルには 'main' 関数が含まれています。プログラム実行の開始と終了がそこで行われます。
//

#include <iostream>
#include "ITKStripTEGApp.h"
#include "prologix_gpibusb.h"
#include"keithley6517A.h"
//#include "stdafx.h"
//int SerialTest();
int main()
{
    std::cout << "Hello World!\n";
//    SerialTest();
//    return 0;
    
    ITKStripTEGApp ap;
    ap.TUSBPIO_test();



    serial_interface* si = new prologix_gpibusb();
    std::string device_name = "COM4";
    si->set_device_name(device_name);
    std::cout << si->get_device_name() << std::endl;
    si->initialize();
    std::cout << "initialized ... " << std::endl;
    if (! si->is_initialized()) {
        std::cout << "[ERROR] failed to create an instance of the serial interface for " << device_name << "." << std::endl;
        return -1;
    }
    else {
        std::cout << "successfully opened.." << std::endl;
    }

    int keithley6517A_1_address = 28;
    double keithley6517A_1_compliance = 10.E-6;
    power_supply* keithley6517A_1 = new keithley6517A(keithley6517A_1_address);
    keithley6517A_1->set_voltage(0.0); //initial voltage
    keithley6517A_1->set_compliance(keithley6517A_1_compliance);
    std::cout << "c1" << std::endl;
    keithley6517A_1->power_on(si);
    std::cout << "c2" << std::endl;    
    keithley6517A_1->power_off(si);
    std::cout << "c3" << std::endl;

    delete keithley6517A_1;

}
/*
int SerialTest(){
// TODO: Specify Virtual COM port
char* port = "COM4";

char cmd[253];
DWORD error = 0;

printf("Command: ");

// Read command from console
//gets( cmd );
std::cin >> cmd;

do
{
    // Open port
    error = PxSerialOpen(port);
    if (error != 0)
    {
        printf("Error %08x opening %s.\n", error, port);
        break;
    }

    // Append CR and LF
    char buffer[256];
    sprintf(buffer, "%s\r\n", cmd);

    // Write command to port
    DWORD written = 0;
    error = PxSerialWrite(buffer, (DWORD)strlen(buffer), &written);
    if (error != 0)
    {
        printf("Error %08x writing %s.\n", error, port);
        break;
    }

    // TODO: Adjust timeout as needed
    const DWORD TIMEOUT = 1500;     // Millisec

    DWORD elapsedTime = 0;
    DWORD lastRead = timeGetTime();

    // Read until TIMEOUT time has elapsed since last
    // successful read.
    while (elapsedTime <= TIMEOUT)
    {
        DWORD bytesRead;

        error = PxSerialRead(buffer, sizeof(buffer) - 1, &bytesRead);
        if (error != 0)
        {
            printf("Error %08x reading %s.\n", error, port);
            break;
        }

        if (bytesRead > 0)
        {
            buffer[bytesRead] = 0;    // Append NULL to print to console
            printf("%s", buffer);

            lastRead = timeGetTime();
        }

        elapsedTime = timeGetTime() - lastRead;
    }
} while (0);

printf("\n");

// Close port
error = PxSerialClose();
if (error != 0)
{
    printf("Error %08x closing %s.\n", error, port);
}

return 0;
    }
    */

// プログラムの実行: Ctrl + F5 または [デバッグ] > [デバッグなしで開始] メニュー
// プログラムのデバッグ: F5 または [デバッグ] > [デバッグの開始] メニュー

// 作業を開始するためのヒント: 
//    1. ソリューション エクスプローラー ウィンドウを使用してファイルを追加/管理します 
//   2. チーム エクスプローラー ウィンドウを使用してソース管理に接続します
//   3. 出力ウィンドウを使用して、ビルド出力とその他のメッセージを表示します
//   4. エラー一覧ウィンドウを使用してエラーを表示します
//   5. [プロジェクト] > [新しい項目の追加] と移動して新しいコード ファイルを作成するか、[プロジェクト] > [既存の項目の追加] と移動して既存のコード ファイルをプロジェクトに追加します
//   6. 後ほどこのプロジェクトを再び開く場合、[ファイル] > [開く] > [プロジェクト] と移動して .sln ファイルを選択します
