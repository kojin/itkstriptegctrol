#include "stdafx.h"
#include "ITKStripTEGApp.h"
#include <stdio.h>
#include <iostream>
#include <cstdlib>


ITKStripTEGApp::ITKStripTEGApp() {
	initialize();
}
int ITKStripTEGApp::initialize() {
	usbpio = new TUSBPIOCtrl(0);
	ivcv = new IVCVCtrl();
	return 0;
}
int ITKStripTEGApp::IVCVCtrol_test() {
	return ivcv->IVCVCtrl_test();
}
void ITKStripTEGApp::TUSBPIO_test() {

	int PortN;
	std::cout << "Select Port No." << std::endl;
	std::cout << "PortA=0,PortB=1,PortC=2" << std::endl;
	std::cin >> PortN;

	usbpio->SetBit(PortN, 0x4);
	usbpio->SetSWon(0);
	system("PAUSE");

	usbpio->SetSWoff();

}