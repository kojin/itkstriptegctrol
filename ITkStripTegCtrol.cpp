// SerialTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "comport.h"
#include <iostream>
#include "ITKStripTEGApp.h"


int main(int argc, _TCHAR* argv[])
    {

    ITKStripTEGApp ap;
    ap.TUSBPIO_test();
    ap.IVCVCtrol_test();

    
    return 0;
    /*
    // TODO: Specify Virtual COM port
    char* port = "COM4";

    char cmd[ 253 ];
    DWORD error = 0;

    printf( "Command: " );

    // Read command from console
    //gets( cmd );
    std::cin >> cmd;

    do
        {
        // Open port
        error = PxSerialOpen( port );
        if ( error != 0 )
            {
            printf( "Error %08x opening %s.\n", error, port );
            break;
            }

        // Append CR and LF
        char buffer[ 256 ];
        sprintf_s( buffer, "%s\r\n", cmd );

        // Write command to port
        DWORD written = 0;
        error = PxSerialWrite( buffer, (DWORD)strlen( buffer ), &written );
        if ( error != 0 )
            {
            printf( "Error %08x writing %s.\n", error, port );
            break;
            }

        // TODO: Adjust timeout as needed
        const DWORD TIMEOUT = 1500;     // Millisec

        DWORD elapsedTime = 0;
        DWORD lastRead = timeGetTime();

        // Read until TIMEOUT time has elapsed since last
        // successful read.
        while ( elapsedTime <= TIMEOUT )
            {
            DWORD bytesRead;

            error = PxSerialRead( buffer, sizeof( buffer ) - 1, &bytesRead );
            if ( error != 0 )
                {
                printf( "Error %08x reading %s.\n", error, port );
                break;
                }

            if ( bytesRead > 0 )
                {
                buffer[ bytesRead ] = 0;    // Append NULL to print to console
                printf( "%s", buffer );

                lastRead = timeGetTime();
                }

            elapsedTime = timeGetTime() - lastRead;
            }
        }
    while ( 0 );

    printf( "\n" );

    // Close port
    error = PxSerialClose();
    if ( error != 0 )
        {
        printf( "Error %08x closing %s.\n", error, port );
        }




   


    return 0;
    */
    }

