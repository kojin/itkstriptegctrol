#include "stdafx.h"
#include "IVCVCtrl.h"

IVCVCtrl::IVCVCtrl() {

}
int IVCVCtrl::Initialize() {



	return 0;
}
int IVCVCtrl::IVCVCtrl_test() {
    serial_interface* si = new prologix_gpibusb();
    std::string device_name = "COM5";
    si->set_device_name(device_name);
    std::cout << si->get_device_name() << std::endl;
    si->initialize();
    std::cout << "initialized ... " << std::endl;
    if (!si->is_initialized()) {
        std::cout << "[ERROR] failed to create an instance of the serial interface for " << device_name << "." << std::endl;
        return -1;
    }
    else {
        std::cout << "successfully opened.." << std::endl;
    }
    /*
    int keithley2410_1_address = 24;
    double keithley2410_1_compliance = 10.E-6;
    //double keithley2410_1_output_voltage = 80.;
    power_supply* keithley2410_1 = new keithley2410(keithley2410_1_address);
    keithley2410_1->set_voltage(0.0); //initial voltage
    keithley2410_1->set_compliance(keithley2410_1_compliance);

//    keithley2410_1->power_on(si);
//    keithley2410_1->power_off(si);
    */

    int keithley6517A_1_address = 29;
    double keithley6517A_1_compliance = 10.E-6;
    power_supply* keithley6517A_1 = new keithley6517A(keithley6517A_1_address);
    keithley6517A_1->set_voltage(0.0); //initial voltage
    keithley6517A_1->set_compliance(keithley6517A_1_compliance);
    std::cout << "c1" << std::endl;
    keithley6517A_1->power_on(si);
    std::cout << "c2" << std::endl;
    keithley6517A_1->power_off(si);
    std::cout << "c3" << std::endl;


    delete keithley6517A_1;
//    delete keithley2410_1;
    return 0;
}