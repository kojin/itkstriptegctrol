#include "stdafx.h"
#include<iostream>
#include<chrono>
#include<thread>
#include<cmath>

#include"serial_interface.h"
#include"hp4192A.h"
#include"SerialCom.h"

int hp4192A::initialize()
{
    return 0;
}
int hp4192A::finalize()
{
    return 0;
}

int hp4192A::reset(serial_interface* si)
{
    return 0;
}

int hp4192A::zeroopen(serial_interface* si)
{
    si->set_address(m_address);
    si->write("ZO1");
    si->make_listener();
    return 0;
}


int hp4192A::zeroshort(serial_interface* si)
{
    si->set_address(m_address);
    si->write("ZS1");
    si->make_listener();
    return 0;
}


int hp4192A::configure(serial_interface* si)
{
    char cmd[MAX];
    si->set_address(m_address);
    si->write("A4"); //DISPLAY A represents capacitance
    si->write("B2"); //DISPLAY B represents degree
    si->write("F0"); //data output format : DISPLAY A/B
    //  si->write("C2"); //when you use series circuit
    //  si->write("C3"); //when you use parallel circuit
    si->write("V1"); //avarege mode
    //  si->write("T1"); //trigger mode INT
    //  si->write("T2"); //trigger mode EXT
    si->make_listener();
    return 0;
}


int hp4192A::config_frequency(serial_interface* si)
{
    char cmd[MAX];
    si->set_address(m_address);
    si->make_listener();
    sprintf(cmd, "FR+%9.4lfEN", m_frequency / 1000);
    si->write(cmd);
    si->make_listener();
    return 0;
}


void hp4192A::read_capacitance_and_degree(serial_interface* si, double& cap, double& deg)
{
    std::string buffer = "";
    //  memset(buffer.c_str(),0x00,MAX);
    si->set_address(m_address);
    si->write("EX"); //Execute
    si->write(":READ?\r\n");
    si->make_talker();
    si->read(buffer);
    sscanf_s(buffer.c_str(), "NCSN%lf,NDFN%lf", &cap, &deg);
    std::cout << buffer << std::endl;
    si->make_listener();
}


void hp4192A::read_capacitance_and_degree2(serial_interface* si, double& cap, double& deg)
{
    std::string buffer2;
    //  memset(buffer.c_str(),0x00,MAX);
    si->set_address(m_address);
    si->write("EX"); //Execute
    si->write(":READ?\r\n");
    si->make_talker();
    si->read2(buffer2);
    sscanf_s(buffer2.c_str(), "NCPN%lf,NDFN%lf", &cap, &deg);
    si->make_listener();
}

void hp4192A::sweep_frequency(serial_interface* si, double& cap, double& deg, double& freq)
{
    std::string buffer;
    const char* dummy;
    char cmd_sf[MAX], cmd_tf[MAX], cmd_pf[MAX];
    //   std::ofstream ofile_freq("data/katsuya/test0.dat", std::ofstream::app);
    //   std::string out_format_freq;
    si->set_address(m_address);
    si->write("A4B2T1F1");
    //  si->write("SF1EN"); //step freq, each unit:[kHz]
    sprintf(cmd_sf, "SF+%9.4lfEN", m_sfrequency / 1000);
    si->write(cmd_sf);
    //  si->write("TF1EN"); //start freq
    sprintf(cmd_tf, "TF+%9.4lfEN", m_tfrequency / 1000);
    si->write(cmd_tf);
    //  si->write("PF100EN"); //stop freq
    sprintf(cmd_pf, "PF+%9.4lfEN", m_pfrequency / 1000);
    si->write(cmd_pf);
    si->write("W1W2"); //W1:sweep auto, W2:step up
    si->write("");
    // for(int ii=0; ii<nos+1; ii++){
    //   si->write("EX");
    //   si->write(":READ?\r\n");
    //   si->make_talker();
    //   si->read(buffer);
    //   std::cout<<buffer<<std::endl;
    //   sscanf(buffer.c_str(),"NCSN%lf,%2sFN%lf,K%lf",&cap,&dummy,&deg,&freq); 
    //   if(ii==0) std::cout<<"freq[Hz]"<<"\t"<<"capacitance[F]"<<"\t"<<"degree[deg]"<<"\t"<<std::endl;
    //   std::cout<<freq*1000<<"\t\t"<<cap<<"\t\t"<<deg<<"\t"<<std::endl;
    //    si->make_listener();
    // }
}