#pragma once
#include "TUSBPIOCtrl.h"
#include "IVCVCtrl.h"

ref class ITKStripTEGApp
{
private:
	TUSBPIOCtrl* usbpio;
	IVCVCtrl* ivcv;
public:
	ITKStripTEGApp();

	int initialize();
	void TUSBPIO_test();
	int IVCVCtrol_test();
};

