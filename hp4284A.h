#ifndef INCLUDED_HP4284A
#define INCLUDED_HP4284A

#include "LCR_meter.h"
#include<iostream>
#include<chrono>
#include<thread>
#include<cmath>

class hp4284A : public LCR_meter
{
 public : 
  hp4284A(){}
  hp4284A(int address){m_address = address;}
  ~hp4284A(){};
  int initialize();
  int finalize();
  int reset(serial_interface *si);
  int zeroopen(serial_interface *si);
  int zeroshort(serial_interface *si);
  int config_frequency(serial_interface *si);
  int configure(serial_interface *si);
  //  double read_capacitance(serial_interface *si);
  //  double read_degree(serial_interface *si);
  //  void read_impedance_and_theta(serial_interface *si, double &cap, double &deg);
  void read_capacitance_and_degree(serial_interface *si, double &cap, double &deg);
  void read_capacitance_and_degree2(serial_interface *si, double &cap, double &deg);
  void sweep_frequency(serial_interface *si, double &cap, double &deg, double &freq);

  std::string get_device_type(){return "hp4284A";};
};






#endif
