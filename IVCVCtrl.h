#pragma once
#include <iostream>
#include "prologix_gpibusb.h"
#include "keithley6517A.h"
#include "keithley2410.h"

class IVCVCtrl
{
private:
public:
	IVCVCtrl();
	~IVCVCtrl() {};
	int Initialize();
	int IVCVCtrl_test();

};

